<?php

if (!defined('LWSPATH'))
    exit('No direct script access allowed');

include_once "controller/LW_Basecontroller.php";

/**
 * Description of LWS_Controller
 *
 * @author nurfadillah
 */
class LWS_Controller extends LW_Basecontroller {
    
    public function __construct() {
        parent::__construct();
    }
}