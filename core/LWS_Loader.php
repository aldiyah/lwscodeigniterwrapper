<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Loader Class
 *
 * Loads views and files
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @author		ExpressionEngine Dev Team
 * @category	Loader
 * @link		http://codeigniter.com/user_guide/libraries/loader.html
 */
class LWS_Loader extends CI_Loader {

    protected $_module = FALSE;
    public $_ci_plugins = array();
    public $_ci_cached_vars = array();

    /**
     * Constructor
     *
     * Sets the path to the view files and gets the initial output buffering level
     */
    public function __construct() {
        parent::__construct();
        $this->_ci_library_paths = array(APPPATH, LWSPATH, BASEPATH);
        $this->_ci_helper_paths = array(APPPATH, LWSPATH, BASEPATH);
        $this->_ci_model_paths = array(APPPATH, LWSPATH);
        $this->_ci_view_paths = array(APPPATH . 'views/' => TRUE, LWSPATH . 'views/' => TRUE);
    }

    public function initialize($controller = NULL) {

        /* set the module name */
        $this->_module = CI::$APP->router->fetch_module();
        
        if (is_a($controller, 'LWS_ModularController')) {

            /* reference to the module controller */
            $this->controller = $controller;

            /* references to ci loader variables */
            foreach (get_class_vars('CI_Loader') as $var => $val) {
                if ($var != '_ci_ob_level') {
                    $this->$var = & CI::$APP->load->$var;
                }
            }
        } else {
            parent::initialize();

            /* autoload module items */
            $this->_autoloader(array());
        }

        /* add this module path to the loader variables */
        $this->_add_module_paths($this->_module);
    }

    public function get_view_paths() {
        return $this->_ci_view_paths;
    }
    
    public function get_modul_name(){
        return $this->_module;
    }

    /**
     * Load Helper
     *
     * This function loads the specified helper file.
     *
     * @param	mixed
     * @return	void
     */
    public function helper($helpers = array()) {
//        if (!defined('ONWRAPPER')) {
//            parent::helper($helpers);
//        }

        $helper_paths = array_reverse($this->_ci_helper_paths);

        foreach ($this->_ci_prep_filename($helpers, '_helper') as $helper) {

            if (isset($this->_ci_helpers[$helper])) {
                continue;
            }

            $ext_helper = APPPATH . 'helpers/' . $helper . '.php';
            if (file_exists($ext_helper)) {
                include_once($ext_helper);
            }


            $ext_helper = APPPATH . 'helpers/' . config_item('subclass_prefix') . $helper . '.php';

            // Is this a helper extension request?
            if (!file_exists($ext_helper)) {
                $base_helper = BASEPATH . 'helpers/' . $helper . '.php';
                $lws_helper = LWSPATH . 'helpers/LWS_' . $helper . '.php';

                $helper_in_basepath_found = TRUE;
                if (!file_exists($base_helper)) {
                    $helper_in_basepath_found = FALSE;
//                    show_error('Unable to load the requested file: helpers/' . $helper . '.php');
                    log_message('ERROR', 'Unable to load the requested file: helpers/' . $helper . '.php');
                }

                $helper_in_lwspath_found = TRUE;
                if (!file_exists($lws_helper)) {
                    $helper_in_lwspath_found = FALSE;
//                    show_error('LWS Unable to load the requested file: helpers/LWS_' . $helper . '.php');
                    log_message('ERROR', 'LWS Unable to load the requested file: helpers/LWS_' . $helper . '.php');
                }

//                include_once($ext_helper);
                if ($helper_in_lwspath_found)
                    include_once($lws_helper);

                if ($helper_in_basepath_found)
                    include_once($base_helper);

                if ($helper_in_basepath_found || $helper_in_lwspath_found) {
                    $this->_ci_helpers[$helper] = TRUE;
                    log_message('debug', 'Helper loaded: ' . $helper);
                }
                continue;
            } else {
                include_once($ext_helper);
            }

            // Try to load the helper
            foreach ($helper_paths as $helper_path) {
                $helper_filename = $helper . '.php';
                if ($helper_path == LWSPATH) {
                    $helper_filename = 'LWS_' . $helper . '.php';
                }

                $helper_exists = file_exists($helper_path . 'helpers/' . $helper_filename);
                log_message('debug', 'Check Exists ' . $helper_filename . ': ' . ($helper_exists ? "Yes" : "No"));
                if ($helper_exists) {
                    include_once($helper_path . 'helpers/' . $helper_filename);

                    $this->_ci_helpers[$helper] = TRUE;
                    log_message('debug', 'Helper loaded: ' . $helper);
                    if ($helper_path != LWSPATH) {
                        break;
                    }
                }

                list($path, $_helper) = LWS_Modules::find($helper_filename, $this->_module, 'helpers/');

                if ($path === FALSE)
                    return parent::helper($helper);

                Modules::load_file($_helper, $path);
                $this->_ci_helpers[$_helper] = TRUE;
            }
        }

        // unable to load the helper
        if (!isset($this->_ci_helpers[$helper]) && !defined('ONWRAPPER')) {
            show_error('Unable to load the requested file: helpers/' . $helper . '.php');
        }
    }

    // --------------------------------------------------------------------

    /**
     * Load class
     *
     * This function loads the requested class.
     * @author mas Feri Harsanto
     * @param	string	the item that is being loaded
     * @param	mixed	any additional parameters
     * @param	string	an optional object name
     * @return	void
     */
    protected function _ci_load_class($class, $params = NULL, $object_name = NULL) {
        parent::_ci_load_class($class, $params, $object_name);

//        if(in_array($class, $this->_ci_classes)){
//            log_message('debug', 'Class Library still loaded: on ' . $class);
//            return TRUE;
//        }

        $class = str_replace('.php', '', trim($class, '/'));
        $CI = & get_instance();

        $subdir = '';
        if (($last_slash = strrpos($class, '/')) !== FALSE) {
            // Extract the path
            $subdir = substr($class, 0, $last_slash + 1);

            // Get the filename from the path
            $class = substr($class, $last_slash + 1);
        }

        $classvar = strtolower($class);
        if (isset($CI->$classvar)) {
            $class_name = strstr($class, 'LWS_') ? ucfirst($class) : 'LWS_' . ucfirst($class);
            $baseclass = BASEPATH . 'libraries/' . $subdir . ucfirst($class) . '.php';
            $class_path = LWSPATH . 'libraries/' . $subdir . $class_name . '.php';

            if (file_exists($class_path)) {
                include_once($class_path);

                if (file_exists($baseclass))
                    include_once($baseclass);

                if (count($this->_ci_loaded_files) > 0) {
                    foreach ($this->_ci_loaded_files as $index => $file) {
                        if (strstr($file, $class)) {
                            $this->_ci_loaded_files[$index] = $class_path;
                        }
                    }
                }
                $class = strtolower($class);
                $this->_ci_classes[$class] = $class;
                if ($params !== NULL) {
                    $CI->$class = new $class_name($params);
                } else {
                    $CI->$class = new $class_name;
                }
                log_message('debug', 'Class Library loaded: ' . $class_name . ' on ' . $class);
            }
        }
        return TRUE;
    }

    public function file_library($library = '') {
        if ($library == '') {
            return FALSE;
        }

        //var_dump(APPPATH.'libraries/'.$library.EXT);
        //var_dump(file_exists(APPPATH.'libraries/'.$library.EXT)); exit;
        // Is this a helper extension request?			
        if (file_exists(APPPATH . 'libraries/' . $library . EXT)) {
            include_once(APPPATH . 'libraries/' . $library . EXT);
        } elseif (file_exists(LWSPATH . 'libraries/' . $library . EXT)) {
            include_once(LWSPATH . 'libraries/' . $library . EXT);
        } elseif (file_exists(BASEPATH . 'libraries/' . $library . EXT)) {
            include_once(BASEPATH . 'libraries/' . $library . EXT);
        } else {
            show_error('Unable to load the requested file: libraries/' . $library . EXT);
        }
        log_message('debug', 'File Library loaded: ' . $library);
    }

    /**
     * Database Loader
     *
     * @access    public
     * @param    string    the DB credentials
     * @param    bool    whether to return the DB object
     * @param    bool    whether to enable active record (this allows us to override the config setting)
     * @return    object
     */
    function database($params = '', $return = FALSE, $active_record = NULL) {
        // Do we even need to load the database class?
        if (class_exists('CI_DB') AND $return == FALSE AND $active_record == NULL AND isset($CI->db) AND is_object($CI->db)) {
            return FALSE;
        }

        require_once(LWSPATH . 'database/LWS_DB' . EXT);

        // Load the DB class
        $result = DB($params, $active_record);
        $params = $result["param"];
        $DB = FALSE;
        if ($result["extended_driver_found"]) {
            $lws_db_driver = 'LWS_DB_' . $params['dbdriver'] . '_driver';
            $DB = new $lws_db_driver($result["param"]);
            unset($lws_db_driver);
        } else {
            // Instantiate the DB adapter
            $driver = 'CI_DB_' . $params['dbdriver'] . '_driver';
            $DB = new $driver($params);
            unset($driver);
        }

        //initiate DB
        if ($DB->autoinit == TRUE) {
            $DB->initialize();
        }

        if (isset($params['stricton']) && $params['stricton'] == TRUE) {
            $DB->query('SET SESSION sql_mode="STRICT_ALL_TABLES"');
        }

        if ($return === TRUE) {
            return $DB;
        }
        // Grab the super object
        $CI = & get_instance();

        // Initialize the db variable.  Needed to prevent
        // reference errors with some configurations
        $CI->db = '';
        CI::$APP->db = $CI->db = $DB;
        return $DB;
    }

    /** Load a module config file * */
    public function config($file = 'config', $use_sections = FALSE, $fail_gracefully = FALSE) {
        return CI::$APP->config->load($file, $use_sections, $fail_gracefully, $this->_module);
    }

    /** Add a module path loader variables * */
    public function _add_module_paths($module = '') {

        if (empty($module))
            return;

        foreach (LWS_Modules::$locations as $location => $offset) {

            /* only add a module path if it exists */
            if (is_dir($module_path = $location . $module . '/') && !in_array($module_path, $this->_ci_model_paths)) {
                array_unshift($this->_ci_model_paths, $module_path);
            }
        }
    }

    /** Load a module language file * */
    public function language($langfile = array(), $idiom = '', $return = FALSE, $add_suffix = TRUE, $alt_path = '') {
        return CI::$APP->lang->load($langfile, $idiom, $return, $add_suffix, $alt_path, $this->_module);
    }

    public function languages($languages) {
        foreach ($languages as $_language)
            $this->language($_language);
    }

    /** Load a module library * */
    public function library($library = '', $params = NULL, $object_name = NULL) {

        if (is_array($library))
            return $this->libraries($library);

        $class = strtolower(basename($library));

        if (isset($this->_ci_classes[$class]) AND $_alias = $this->_ci_classes[$class])
            return CI::$APP->$_alias;

        ($_alias = strtolower($object_name)) OR $_alias = $class;

        list($path, $_library) = LWS_Modules::find($library, $this->_module, 'libraries/');

        /* load library config file as params */
        if ($params == NULL) {
            list($path2, $file) = LWS_Modules::find($_alias, $this->_module, 'config/');
            ($path2) AND $params = LWS_Modules::load_file($file, $path2, 'config');
        }

        if ($path === FALSE) {

            $this->_ci_load_class($library, $params, $object_name);
            $_alias = $this->_ci_classes[$class];
        } else {

            LWS_Modules::load_file($_library, $path);

            $library = ucfirst($_library);
            CI::$APP->$_alias = new $library($params);

            $this->_ci_classes[$class] = $_alias;
        }

        return CI::$APP->$_alias;
    }

    /** Load an array of libraries * */
    public function libraries($libraries) {
        foreach ($libraries as $_library)
            $this->library($_library);
    }

    /** Load a module model * */
    public function model($model, $object_name = NULL, $connect = FALSE) {

        if (is_array($model))
            return $this->models($model);

        ($_alias = $object_name) OR $_alias = basename($model);

        if (in_array($_alias, $this->_ci_models, TRUE))
            return CI::$APP->$_alias;

        /* check module */
        list($path, $_model) = LWS_Modules::find(strtolower($model), $this->_module, 'models/');

        if ($path == FALSE) {

            /* check application & packages */
            parent::model($model, $object_name, $connect);
        } else {

            class_exists('CI_Model', FALSE) OR load_class('Model', 'core');

            if ($connect !== FALSE AND ! class_exists('CI_DB', FALSE)) {
                if ($connect === TRUE)
                    $connect = '';
                $this->database($connect, FALSE, TRUE);
            }

            Modules::load_file($_model, $path);

            $model = ucfirst($_model);
            CI::$APP->$_alias = new $model();

            $this->_ci_models[] = $_alias;
        }

        return CI::$APP->$_alias;
    }

    /** Load an array of models * */
    public function models($models) {
        foreach ($models as $_model)
            $this->model($_model);
    }

    /** Load a module controller * */
    public function module($module, $params = NULL) {

        if (is_array($module))
            return $this->modules($module);

        $_alias = strtolower(basename($module));

        CI::$APP->$_alias = Modules::load(array($module => $params));
        return CI::$APP->$_alias;
    }

    /** Load an array of controllers * */
    public function modules($modules) {
        foreach ($modules as $_module)
            $this->module($_module);
    }

    /** Load a module view * */
    public function view($view, $vars = array(), $return = FALSE) {
        list($path, $_view) = LWS_Modules::find($view, $this->_module, 'views/');

        if ($path != FALSE) {
            $this->_ci_view_paths = array($path => TRUE) + $this->_ci_view_paths;
            $view = $_view;
        }

        return $this->_ci_load(array('_ci_view' => $view, '_ci_vars' => $this->_ci_object_to_array($vars), '_ci_return' => $return));
    }

    public function _ci_load($_ci_data) {

        extract($_ci_data);

        $result_view_path = FALSE;
        if (isset($_ci_view)) {

            $_ci_path = '';

            /* add file extension if not provided */
            $_ci_file = (pathinfo($_ci_view, PATHINFO_EXTENSION)) ? $_ci_view : $_ci_view . EXT;
            
            if($this->_module){
                $this->_ci_view_paths = array(APPPATH . 'views/' => TRUE, LWSPATH . 'views/' => TRUE);
                
                $result_view_path = LWS_Modules::find_view(DIRECTORY_SEPARATOR . basename($_ci_file, EXT), $this->_module, $_ci_vars["active_modul"], TRUE);
                
                if($result_view_path && $result_view_path[0]){
                    $this->_ci_view_paths[$result_view_path[0]] = TRUE;
                    $_ci_file = str_replace("\\","",$result_view_path[1]);
                }
            }
            
            foreach ($this->_ci_view_paths as $path => $cascade) {
                if (file_exists($view = $path . $_ci_file)) {
                    $_ci_path = $view;
                    break;
                }

                if (!$cascade)
                    break;
            }
            unset($this->_ci_view_paths[$result_view_path[0]]);
            unset($result_view_path);
        } elseif (isset($_ci_path)) {

            $_ci_file = basename($_ci_path);
            if (!file_exists($_ci_path))
                $_ci_path = '';
        }

        if (empty($_ci_path))
            show_error('Unable to load the requested file: ' . $_ci_file);

        if (isset($_ci_vars))
            $this->_ci_cached_vars = array_merge($this->_ci_cached_vars, (array) $_ci_vars);

        extract($this->_ci_cached_vars);

        
        ob_start();

        if ((bool) @ini_get('short_open_tag') === FALSE AND CI::$APP->config->item('rewrite_short_tags') == TRUE) {
            echo eval('?>' . preg_replace("/;*\s*\?>/", "; ?>", str_replace('<?=', '<?php echo ', file_get_contents($_ci_path))));
        } else {
            include($_ci_path);
        }

        log_message('debug', 'File loaded: ' . $_ci_path);

        if ($_ci_return == TRUE)
            return ob_get_clean();

        if (ob_get_level() > $this->_ci_ob_level + 1) {
            ob_end_flush();
        } else {
//            CI::$APP->output->append_output(ob_get_clean());
        }
    }

    /** Autoload module items * */
    public function _autoloader($autoload) {

        $path = FALSE;

        if ($this->_module) {

            list($path, $file) = LWS_Modules::find('constants', $this->_module, 'config/');

            /* module constants file */
            if ($path != FALSE) {
                include_once $path . $file . EXT;
            }

            list($path, $file) = LWS_Modules::find('autoload', $this->_module, 'config/');

            /* module autoload file */
            if ($path != FALSE) {
                $autoload = array_merge(LWS_Modules::load_file($file, $path, 'autoload'), $autoload);
            }
        }

        /* nothing to do */
        if (count($autoload) == 0)
            return;

        /* autoload config */
        if (isset($autoload['config'])) {
            foreach ($autoload['config'] as $config) {
                $this->config($config);
            }
        }

        /* autoload helpers, plugins, languages */
        foreach (array('helper', 'language') as $type) {
            if (isset($autoload[$type])) {
                foreach ($autoload[$type] as $item) {
                    $this->$type($item);
                }
            }
        }

        /* autoload database & libraries */
        if (isset($autoload['libraries'])) {
            if (in_array('database', $autoload['libraries'])) {
                /* autoload database */
                if (!$db = CI::$APP->config->item('database')) {
                    $db['params'] = 'default';
                    $db['active_record'] = TRUE;
                }
                $this->database($db['params'], FALSE, $db['active_record']);
                $autoload['libraries'] = array_diff($autoload['libraries'], array('database'));
            }

            /* autoload libraries */
            foreach ($autoload['libraries'] as $library) {
                $this->library($library);
            }
        }

        /* autoload models */
        if (isset($autoload['model'])) {
            foreach ($autoload['model'] as $model => $alias) {
                (is_numeric($model)) ? $this->model($alias) : $this->model($model, $alias);
            }
        }

        /* autoload module controllers */
        if (isset($autoload['modules'])) {
            foreach ($autoload['modules'] as $controller) {
                ($controller != $this->_module) AND $this->module($controller);
            }
        }
    }

}

(class_exists('CI', FALSE)) OR require dirname(__FILE__) . '/controller/CI.php';
/* End of file Loader.php */
/* Location: ./system/core/Loader.php */