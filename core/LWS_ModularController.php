<?php

if (!defined('LWSPATH'))
    exit('No direct script access allowed');

require dirname(__FILE__) . '/controller/Base.php';
//require dirname(__FILE__) . '/../LWS.php';

/**
 * Description of LWS_ModularController
 *
 * @author nurfadillah
 */
class LWS_ModularController {

    public $autoload = array();

    public function __construct() {
        $class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
        log_message('debug', $class . " LWS_ModularController Initialized");
        LWS_Modules::$registry[strtolower($class)] = $this;

        /* copy a loader instance and initialize */
        $this->load = clone load_class('Loader');
        $this->load->initialize($this);

        /* autoload module items */
        $this->load->_autoloader($this->autoload);
        
//        $class = new ReflectionClass("LWS_Controller");
//        var_dump($class);exit;
    }
    
    public function _remap($method, $params = array()) {
        if (method_exists($this, $method)) {
            $this->_action = $method;
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
    }

    public function __get($class) {
        return CI::$APP->$class;
    }

}
