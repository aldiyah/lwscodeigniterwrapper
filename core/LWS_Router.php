<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Router Class
 *
 * Parses URIs and determines routing
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @author		ExpressionEngine Dev Team
 * @category	Libraries
 * @link		http://codeigniter.com/user_guide/general/routing.html
 * 
 */
class LWS_Router extends CI_Router {

    protected $module;

    public function __construct() {
        parent::__construct();
    }

    public function fetch_module() {
        return $this->module;
    }

    function _check_is_dir($base_path, $segments) {
        if (is_dir($base_path . 'controllers/' . $segments[0])) {
            // Set the directory and remove it from the segment array
            $this->set_directory($segments[0]);
            $segments = array_slice($segments, 1);

            if (count($segments) > 0) {
                // Does the requested controller exist in the sub-folder?
                if (!file_exists($base_path . 'controllers/' . $this->fetch_directory() . $segments[0] . '.php')) {
                    if (!empty($this->routes['404_override'])) {
                        $x = explode('/', $this->routes['404_override']);

                        $this->set_directory('');
                        $this->set_class($x[0]);
                        $this->set_method(isset($x[1]) ? $x[1] : 'index');

                        return $x;
                    } else {
                        show_404($this->fetch_directory() . $segments[0]);
                    }
                }
            } else {
                // Is the method being specified in the route?
                if (strpos($this->default_controller, '/') !== FALSE) {
                    $x = explode('/', $this->default_controller);

                    $this->set_class($x[0]);
                    $this->set_method($x[1]);
                } else {
                    $this->set_class($this->default_controller);
                    $this->set_method('index');
                }

                // Does the default controller exist in the sub-folder?
                if (!file_exists($base_path . 'controllers/' . $this->fetch_directory() . $this->default_controller . '.php')) {
                    $this->directory = '';
                    return array();
                }
            }

            return $segments;
        }
        return FALSE;
    }

    /**
     * Validates the supplied segments.  Attempts to determine the path to
     * the controller.
     *
     * @access	private
     * @param	array
     * @return	array
     */
    function _validate_request($segments) {
        if (count($segments) == 0) {
            return $segments;
        }

        // Does the requested controller exist in the root folder?
        if (file_exists(APPPATH . 'controllers/' . $segments[0] . '.php')) {
            return $segments;
        }

        // Is the controller in a sub-folder?
        $dir_found = $this->_check_is_dir(APPPATH, $segments);
        if ($dir_found) {
            return $dir_found;
        }

        //Is The controllers is LWS system controller ?
        if (file_exists(LWSPATH . 'controllers/' . $segments[0] . '.php')) {
            return $segments;
        }

        // Is the controller in a LWSPATH sub-folder?
        $dir_found = $this->_check_is_dir(LWSPATH, $segments);
        if ($dir_found) {
            return $dir_found;
        }

        if ($located = $this->locate($segments))
            return $located;

        // If we've gotten this far it means that the URI does not correlate to a valid
        // controller class.  We will now see if there is an override
        if (isset($this->routes['404_override']) && !empty($this->routes['404_override'])) {
            $x = explode('/', $this->routes['404_override']);

            $this->set_class($x[0]);
            $this->set_method(isset($x[1]) ? $x[1] : 'index');

            return $x;
        }


        // Nothing else to do at this point but show a 404
        show_404($segments[0]);
    }

    /** Locate the controller * */
    public function locate($segments) {

        $this->module = '';
        $this->directory = '';
        $ext = $this->config->item('controller_suffix') . EXT;

        /* use module route if available */
        if (isset($segments[0]) AND $routes = LWS_Modules::parse_routes($segments[0], implode('/', $segments))) {
            $segments = $routes;
        }

        /* get the segments array elements */
        list($module, $directory, $controller) = array_pad($segments, 3, NULL);

        /* check modules */
        foreach (LWS_Modules::$locations as $location => $offset) {

            /* module exists? */
            if (is_dir($source = $location . $module . '/controllers/')) {

                $this->module = $module;
                $this->directory = $offset . $module . '/controllers/';

                /* module sub-controller exists? */
                if ($directory AND is_file($source . $directory . $ext)) {
                    return array_slice($segments, 1);
                }

                /* module sub-directory exists? */
                if ($directory AND is_dir($source . $directory . '/')) {

                    $source = $source . $directory . '/';
                    $this->directory .= $directory . '/';

                    /* module sub-directory controller exists? */
                    if (is_file($source . $directory . $ext)) {
                        return array_slice($segments, 1);
                    }

                    /* module sub-directory sub-controller exists? */
                    if ($controller AND is_file($source . $controller . $ext)) {
                        return array_slice($segments, 2);
                    }
                }

                /* module controller exists? */
                if (is_file($source . $module . $ext)) {
                    return $segments;
                }
            }
        }

        /* application controller exists? */
        if (is_file(APPPATH . 'controllers/' . $module . $ext)) {
            return $segments;
        }

        /* application sub-directory controller exists? */
        if ($directory AND is_file(APPPATH . 'controllers/' . $module . '/' . $directory . $ext)) {
            $this->directory = $module . '/';
            return array_slice($segments, 1);
        }

        /* application sub-directory default controller exists? */
        if (is_file(APPPATH . 'controllers/' . $module . '/' . $this->default_controller . $ext)) {
            $this->directory = $module . '/';
            return array($this->default_controller);
        }
    }

}
